//
//  Communicator.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class Communicator: NSObject, CommunicatorProtocol {
    
    private struct Constant {
        static let errorDomain = "CommunicatorErrorDomain"
    }
    
    func request(url: String, method: RequestMethod, parameters: [String:String]?, completion:@escaping (_ response: Any?, _ error: NSError?)-> Void) {
        
        let alamofireMethod :HTTPMethod
        
        //TODO: define const
        switch method {
        case .post:
            alamofireMethod = .post
        case .get:
            alamofireMethod = .get
        case .put:
            alamofireMethod = .put
        case .delete:
            alamofireMethod = .delete
        }
        
        Alamofire.request(url, method: alamofireMethod, parameters: parameters, encoding: URLEncoding.default).responseJSON { response in
            
            if let json = response.result.value {
                print("JSON: \(json)")
                completion(json, nil)
            } else {
                completion(nil, NSError(domain: Constant.errorDomain,code: 0))
            }
        }
    }
}
