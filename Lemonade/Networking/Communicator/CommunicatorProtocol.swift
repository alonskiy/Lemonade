//
//  CommunicatorProtocol.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation

enum RequestMethod {
    case post
    case get
    case put
    case delete
}

protocol CommunicatorProtocol {
    
    func request(url: String, method: RequestMethod, parameters: [String:String]?, completion:@escaping (_ response: Any?, _ error: NSError?)-> Void)
}
