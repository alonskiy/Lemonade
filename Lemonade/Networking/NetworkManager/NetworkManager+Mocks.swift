//
//  NetworkManager+Mocks.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

extension NetworkManager {
    
    func getMockedFriends() -> [Person] {

        var friend1 = Person()
        friend1.personId = 1
        friend1.name = "Abbey Lee"
        friend1.picUrl = "abbeyLee.png"
        friend1.birthday = Date()
        friend1.gender = .female
        
        var friend2 = Person()
        friend2.personId = 2
        friend2.name = "Ashley Welson"
        friend2.picUrl = "ashley.png"
        friend2.birthday = Date()
        friend2.gender = .female
        
        var friend3 = Person()
        friend3.personId = 3
        friend3.name = "Ayrton Senna"
        friend3.picUrl = "ayrtonSenna.png"
        friend3.birthday = Date()
        friend3.gender = .male
        
        var friend4 = Person()
        friend4.personId = 4
        friend4.name = "Bruce Dickinson"
        friend4.picUrl = "bruce.png"
        friend4.birthday = Date()
        friend4.gender = .male
        
        var friend5 = Person()
        friend5.personId = 5
        friend5.name = "Charlotte Nelson"
        friend5.picUrl = "charlotte.png"
        friend5.birthday = Date()
        friend5.gender = .female
        
        var friend6 = Person()
        friend6.personId = 6
        friend6.name = "Nils Anderson"
        friend6.picUrl = "nils.png"
        friend6.birthday = Date()
        friend6.gender = .male
        
        return [friend1, friend2, friend3, friend4, friend5, friend6]
        
    }
    
    func getMockedShortList() -> [Product] {
        return FavList.sharedInstance.list
    }
    
    func getMockedSuggestions() -> [Product] {
        
        var product1 = Product()
        product1.productId = 1;
        product1.name = "Barbecook Schurze mit Grillzubehor"
        product1.imageHighResUrl = "download1.png"
        product1.price = 49.00
        product1.desc = "Barbecook Schurze mit Grillzubehor description: Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings...Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings...Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings...Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings..."
        product1.url = "https://siroop.ch/baumarkt-garten/grill-feuerstelle/grillbesteck/barbecook-schuerze-mit-grillzubehoer-080478"
        
        var product2 = Product()
        product2.productId = 2;
        product2.name = "Barbecook Gabel aus Edelstahl"
        product2.imageHighResUrl = "download2.png"
        product2.price = 26.95
        product2.desc = "Barbecook Gabel aus Edelstahl description: Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings..."
        product2.url = "https://siroop.ch/baumarkt-garten/grill-feuerstelle/grillbesteck/barbecook-gabel-aus-edelstahl-0278891"
        
        var product3 = Product()
        product3.productId = 3;
        product3.name = "Barbecook Grillbesteck"
        product3.imageHighResUrl = "download3.png"
        product3.price = 20.95
        product3.desc = "Barbecook Grillbesteck description: Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings..."
        product3.url = "https://siroop.ch/baumarkt-garten/grill-feuerstelle/grillbesteck/barbecook-grillbesteck-0306077"
        
        var product4 = Product()
        product4.productId = 4;
        product4.name = "Neft Vodka White Barrel"
        product4.imageHighResUrl = "download4.png"
        product4.price = 84.00
        product4.desc = "Neft Vodka White Barrel description: Tune in as they cover everything from changes in Swift 4 to server-side Swift to compiler stability, and make valiant attempts at euphemisms for other languages' *ahem* shortcomings..."
        product4.url = "https://siroop.ch/lebensmittel-getraenke/spirituosen/vodka/neft-vodka-white-barrel-07l-0614354"
        
        return [product1, product2, product3, product4]
    }
}
