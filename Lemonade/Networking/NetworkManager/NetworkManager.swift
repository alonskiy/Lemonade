//
//  NetworkManager.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {
    
    private let baseURL = "https://api.siroop.ch/"
    private let apiKey = "8ccd66bb1265472cbf8bed4458af4b07"
    private var communicator: CommunicatorProtocol
    private var parser: ParserProtocol
    
    private struct Constant {
        static let mockedFriends = true
        static let mockedShortList = true
        static let mockedSuggestions = true
    }
    
    init(communicator: CommunicatorProtocol, parser: ParserProtocol) {
        self.communicator = communicator
        self.parser = parser
    }
    
    func getAllFriends(completion: @escaping ([Person]) -> Void) {
        
        if Constant.mockedFriends {
            completion(getMockedFriends())
        } else {
            self.communicator.request(url: "\(baseURL)friends/", method: .get, parameters: nil) { (response, error) in
                guard error == nil else {
                    completion([Person]())
                    return
                }
                //parse
                completion(self.parser.parsePersons(data: response))
            }
        }
    }
    
    func getShortList(for person: Person, completion: @escaping ([Product]) -> Void) {
        
        if Constant.mockedShortList {
            completion(getMockedShortList())
        } else {
            
            if let personId = person.personId {
                self.communicator.request(url: "\(baseURL)/person/\(personId)/products/", method: .get, parameters:nil) { (response, error) in
                    guard error == nil else {
                        completion([Product]())
                        return
                    }
                    //parse
                    completion(self.parser.parseProducts(data: response))
                }

            } else {
                completion([Product]())
            }
            
        }
    }
    
    func getSuggestions(quizResult: QuizResult, limit: Int, completion: @escaping ([Product]) -> Void) {
        
        if Constant.mockedSuggestions {
            completion(getMockedSuggestions())
        } else {
            let keywords: String? = SuggestionsManager.generateKeywordsFromQuiz(quizResult: quizResult)
            
            if let keywords = keywords {
                let encodedKeywords = keywords.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
                
                let url = "\(baseURL)product/search/?query=\(encodedKeywords ?? "")&limit=\(limit)&apikey=\(apiKey)"
                
                self.communicator.request(url: url, method: .get, parameters: nil) { (response, error) in
                    guard error == nil else {
                        completion([Product]())
                        return
                    }
                    
                    //parse
                    completion(self.parser.parseProducts(data: response))
                }
            } else {
                completion([Product]())
            }
        }
    }
}
