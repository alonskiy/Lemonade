//
//  Person.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation

struct Person {
    enum Gender: Int {
        case male = 0
        case female
    }
    
    var personId: Int?
    var name: String?
    var picUrl: String?
    var birthday: Date?
    var gender: Gender?
}
