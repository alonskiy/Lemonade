//
//  QuizResult.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

class QuizResult: NSObject {
    var natureFactor: Int?
    var activeFactor: Int?
    var funFactor: Int?
    var ageFactor: Int?
}
