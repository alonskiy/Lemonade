//
//  Product.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//


struct Product {
    var productId: Int?
    var desc: String?
    var ean: String?
    var price: Float?
    var imageHighResUrl: String?
    var imageLowResUrl: String?
    var thumbnailUrl: String?
    var categories: Array<String>?
    var sku: String?
    var name: String?
    var url: String?
    var tags: Array<String>?
    var l3CategoryUrl: String?
    var brand: String?
}
