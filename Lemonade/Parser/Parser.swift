//
//  Parser.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit
import SwiftyJSON

class Parser: NSObject, ParserProtocol {

    func parsePersons(data: Any?) -> [Person] {
        return [Person]()
    }
    
    func parseProducts(data: Any?) -> [Product] {
        
        var result = [Product]()
        if let data = data {
            let json = JSON(data)
            
            for productJson in json.arrayValue {
                var product = Product()

                product.productId = productJson["name"].int
                product.desc = productJson["description"].stringValue
                product.ean = productJson["ean"].stringValue
                product.price = productJson["price"].float
                let images = productJson["images"].dictionaryObject
                product.imageHighResUrl = images?["highres"] as? String
                product.imageLowResUrl = images?["lowres"] as? String
                product.thumbnailUrl = images?["thumbnails"] as? String
                product.categories = productJson["categories"].arrayObject as? Array<String>
                product.sku = productJson["sku"].stringValue
                product.name = productJson["name"].stringValue
                product.url = productJson["url"].stringValue
                product.tags = productJson["tags"].arrayObject as? Array<String>
                product.l3CategoryUrl = productJson["l3_category_url"].stringValue
                product.brand = productJson["brand"].stringValue
                
                result.append(product)
            }
        }
        
        return  result;
    }
}
