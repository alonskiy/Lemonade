//
//  ParserProtocol.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

protocol ParserProtocol {
    
    func parsePersons(data: Any?) -> [Person]
    
    func parseProducts(data: Any?) -> [Product]
}
