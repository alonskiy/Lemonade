//
//  AppDelegate+Appearance.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

extension AppDelegate {

    func setupAppearance() {
        setupNavBarAppearance()
    }
    
    private func setupNavBarAppearance() {
        let appearance = UINavigationBar.appearance()
        let attributes = [NSFontAttributeName: UIFont.lmnTextStyle2Font(), NSForegroundColorAttributeName: UIColor.lmnGunmetal]
        appearance.titleTextAttributes = attributes
    }
}
