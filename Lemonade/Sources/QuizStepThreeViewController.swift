//
//  QuizStepThreeViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class QuizStepThreeViewController: UIViewController {
  
    var person: Person!

    @IBOutlet weak var cookingActivityView: ActionView!
    @IBOutlet weak var sportActivityView: ActionView!
    @IBOutlet weak var gamingActivityView: ActionView!
    @IBOutlet weak var relaxingActivityView: ActionView!
    @IBOutlet var activityViews: [ActionView]!
    
    @IBOutlet weak var questionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityViews.forEach { $0.delegate = self }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        questionLabel.text = self.question
    }
    
    private var question: String {
        
        guard let gender = person?.gender else {
            return "What do you see it doing right now?"
        }
        
        switch gender {
        case .male:
            return "What do you see him doing right now?"
        case .female:
            return "What do you see her doing right now?"
        }
    }
}

extension QuizStepThreeViewController: ActionViewDelegate {
    
    func tapDidOccur(for actionView: ActionView) {
        switch actionView {
        case cookingActivityView:
            sportActivityView.selected = false
            gamingActivityView.selected = false
            relaxingActivityView.selected = false
            
        case sportActivityView:
            cookingActivityView.selected = false
            gamingActivityView.selected = false
            relaxingActivityView.selected = false
            
        case gamingActivityView:
            sportActivityView.selected = false
            cookingActivityView.selected = false
            relaxingActivityView.selected = false
            
        case relaxingActivityView:
            sportActivityView.selected = false
            gamingActivityView.selected = false
            cookingActivityView.selected = false
            
        default: ()
        }
    }
}
