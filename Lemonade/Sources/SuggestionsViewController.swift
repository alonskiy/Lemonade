//
//  SuggestionsViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class SuggestionsViewController: UIViewController, SuggestionCellDelegate {
    
    struct Constant {
        static let suggestionCellId = "suggestionCellReuseId"
        static let showDetailsSegue = "showProductDetailsSegue"
    }
    
    private var currentIndex: Int {
        guard self.collectionView.frame.size.width > 0 else {
            return 0
        }
        
        return Int(self.collectionView.contentOffset.x / self.collectionView.frame.size.width)
    }
    
    @IBOutlet weak var addButton: RoundRectButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var countLabel: UILabel!
    
    @IBAction func onDoneButtonDidPress(_ sender: Any) {
        self.dismiss(animated: true)
    }

    @IBAction func onAddItem(_ sender: Any) {
        let currentProduct = products[currentIndex]
        
        if !FavList.sharedInstance.isProductInTheList(currentProduct) {
            FavList.sharedInstance.list.append(currentProduct)
        }
        
        updateViewForCurrentProduct()
    }
    
    fileprivate var products = [Product]()
    private let networkManager = NetworkManager(communicator: Communicator(), parser: Parser())

    override func viewDidLoad() {
        super.viewDidLoad()
        loadSuggestions()
    }

    private func loadSuggestions() {
        //Get real results in the future
        let quizResult = QuizResult()
        let limitOfResults = 20
        
        self.networkManager.getSuggestions(quizResult: quizResult, limit: limitOfResults) { products in
            self.products.append(contentsOf: products)
            self.updateViewForCurrentProduct()
            self.collectionView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.showDetailsSegue {
            
            guard self.currentIndex < products.count else {
                return
            }
            
            let viewModel = ProductDetailsViewModel(products[self.currentIndex])
            let viewController = segue.destination as! ProductDetailsViewController
            viewController.viewModel = viewModel
        }
    }    
}

extension SuggestionsViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            fatalError("unsupported layout")
        }
        
        let size = flowLayout.itemSize
        return CGSize(width: collectionView.bounds.width, height: size.height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.suggestionCellId, for: indexPath) as! SuggestionCell

        let currentProduct = products[indexPath.row]
        
        if let imageName = currentProduct.imageHighResUrl {
            cell.productImageView.image = UIImage(named: imageName)
        }
        
        if let price = currentProduct.price {
            cell.productPriceLabel.text = String(format: "%.2f", price)
        }
        
        cell.productNameLabel.text = currentProduct.name
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
       updateViewForCurrentProduct()
    }
    
    func updateViewForCurrentProduct() {
        
        if !products.isEmpty {
            let currentIndex = Int(self.collectionView.contentOffset.x / self.collectionView.frame.size.width)
            
            countLabel.text = "\(currentIndex + 1) / \(products.count)"
            
            let currentProduct = products[currentIndex]
            
            if FavList.sharedInstance.isProductInTheList(currentProduct) {
                addButton.setTitle(nil, for: .normal)
                addButton.setImage(#imageLiteral(resourceName: "checkMark"), for: .normal)
            } else {
                addButton.setTitle("Add to List", for: .normal)
                addButton.setImage(nil, for: .normal)
            }
        } else {
            self.countLabel.text = "0"
            addButton.setTitle(nil, for: .normal)
            addButton.setImage(nil, for: .normal)
        }
    }
    
    func onBuy(suggestionCell: SuggestionCell) {
        let currentProduct = self.products[suggestionCell.tag]
        if let url = currentProduct.url,  let requestUrl = URL(string: url) {
            UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
        }
    }
}
