//
//  ShortListViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class ShortListViewController: UIViewController, ShortListCellDelegate {
    
    struct Constant {
        static let shortListCellId = "shortListCellReuseId"
        static let showDetailsSegue = "showProductDetailsSegue"
        static let showQuizSegue = "showQuizSegue"
    }
    
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyListPlaceholder: UIView!
    @IBOutlet weak var suggestMoreGiftsContainer: UIView!
    
    private var person: Person!
    func setupDependency(_ person: Person) {
        self.person = person
    }
    
    fileprivate var products = [Product]()
    private let networkManager = NetworkManager(communicator: Communicator(), parser: Parser())
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let name = person?.name {
            self.navigationItem.title = "Gifts for \(name)"
            self.introLabel.text = "Pass fast and fun quiz to get suggestions for \(name)."
        }
        
        loadShortList()
    }
    
    private func loadShortList() {
        self.networkManager.getShortList(for: person) { [weak self] products in
            
            // TODO: instead of showing/hiding this stuff it's better to have separate viewController
            if products.isEmpty {
                self?.tableView.isHidden = true
                self?.emptyListPlaceholder.isHidden = false
                self?.suggestMoreGiftsContainer.isHidden = true
                self?.navigationItem.rightBarButtonItems?.forEach() { item in
                    item.isEnabled = false
                }
            }
            else {
                self?.tableView.isHidden = false
                self?.emptyListPlaceholder.isHidden = true
                self?.suggestMoreGiftsContainer.isHidden = false
                self?.navigationItem.rightBarButtonItems?.forEach() { item in
                    item.isEnabled = true
                }

                self?.products = products
                self?.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.showDetailsSegue {
            guard let indexPath = self.tableView.indexPathForSelectedRow else {
                fatalError("selected row is expected here")
            }
            
            let viewModel = ProductDetailsViewModel(products[indexPath.row])
            let viewController = segue.destination as! ProductDetailsViewController
            viewController.viewModel = viewModel
        }
        else if segue.identifier == Constant.showQuizSegue {
            let navController = segue.destination as! UINavigationController
            guard let viewController = navController.viewControllers.first as? QuizStepOneViewController else {
                return
            }
            viewController.person = person
        }
    }
}

extension ShortListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constant.shortListCellId, for: indexPath) as! ShortListCell
        
        let currentProduct = products[indexPath.row]
        
        if let imageName = currentProduct.imageHighResUrl {
            cell.productImageView.image = UIImage(named: imageName)
        }
        
        if let price = currentProduct.price {
             cell.productPriceLabel.text = String(format: "%.2f", price)
        }
        
        cell.productNameLabel.text = currentProduct.name
        
        cell.tag = indexPath.row
        
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func onBuy(shortListCell: ShortListCell) {
        let currentProduct = self.products[shortListCell.tag]
        if let url = currentProduct.url,  let requestUrl = URL(string: url) {
            UIApplication.shared.open(requestUrl, options: [:], completionHandler: nil)
        }
    }
}
