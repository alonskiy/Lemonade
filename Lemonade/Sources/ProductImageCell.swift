//
//  ProductImageCell.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class ProductImageCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        productImageView.image = nil
    }
}
