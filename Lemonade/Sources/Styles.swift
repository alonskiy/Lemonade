//
//  Styles.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

// Color palette

extension UIColor {
    class var lmnPaleGrey: UIColor {
        return UIColor(red: 238.0 / 255.0, green: 243.0 / 255.0, blue: 247.0 / 255.0, alpha: 1.0)
    }
    
    class var lmnTealish: UIColor {
        return UIColor(red: 35.0 / 255.0, green: 189.0 / 255.0, blue: 220.0 / 255.0, alpha: 1.0)
    }
    
    class var lmnGunmetal: UIColor {
        return UIColor(red: 64.0 / 255.0, green: 80.0 / 255.0, blue: 89.0 / 255.0, alpha: 1.0)
    }
    
    class var lmnPaleGreyTwo: UIColor {
        return UIColor(red: 221.0 / 255.0, green: 232.0 / 255.0, blue: 238.0 / 255.0, alpha: 1.0)
    }
}

// Text styles

extension UIFont {
    class func lmnTextStyle3Font() -> UIFont {
        return UIFont.systemFont(ofSize: 18.0, weight: UIFontWeightSemibold)
    }
    
    class func lmnTextStyle2Font() -> UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: UIFontWeightSemibold)
    }
    
    class func lmnTextStyleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: UIFontWeightSemibold)
    }
}
