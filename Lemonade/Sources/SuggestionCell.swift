//
//  SuggestionCell.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

protocol SuggestionCellDelegate{
    func onBuy(suggestionCell: SuggestionCell)
}

class SuggestionCell: UICollectionViewCell {
    
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    
    var delegate: SuggestionCellDelegate?
    
    @IBAction func onBuy(_ sender: Any) {
        delegate?.onBuy(suggestionCell: self)
    }
}
