//
//  QuizStepTwoViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class QuizStepTwoViewController: UIViewController {
    
    struct Constant {
        static let segue = "quizActivitiesSegue"
    }
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var person: Person!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let name = person.name {
            descriptionLabel.text = "Some of the \(name)`s data is missed, fill the next data, please:"
        }
        else {
            descriptionLabel.text = "Some of the data is missed, fill the next data, please:"
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Constant.segue, let controller = segue.destination as? QuizStepThreeViewController else {
            return
        }
        
        controller.person = person
    }
}
