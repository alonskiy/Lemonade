//
//  TimelineFlow.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class TimelineFlowLayout: UICollectionViewFlowLayout {

    struct Constant {
        static let calendarHeadertKind = "CalendarHeader"
        static let headerSize = CGSize(width: 69, height: 53)
    }
    
    override func prepare() {
        super.prepare()
        
        self.headerReferenceSize = Constant.headerSize
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let superAttributes = super.layoutAttributesForElements(in: rect) else {
            return super.layoutAttributesForElements(in: rect)
        }
        
        var superAttributesCopy = superAttributes.flatMap({ $0.copy() as? UICollectionViewLayoutAttributes })
        
        var sectionIndicesToAdd = Set<Int>()
        var existingSectionIndices = Set<Int>()
        
        superAttributesCopy.forEach { layoutAttributes in
            
            if let representedElementKind = layoutAttributes.representedElementKind,
                representedElementKind == Constant.calendarHeadertKind {
                
                guard let attributes = layoutAttributesForSupplementaryView(ofKind: Constant.calendarHeadertKind, at: layoutAttributes.indexPath) else {
                    fatalError("couldn't find expected attributes")
                }
                
                layoutAttributes.frame = attributes.frame
                
                existingSectionIndices.insert(attributes.indexPath.section)
            }
            else {
                guard let attributes = layoutAttributesForItem(at: layoutAttributes.indexPath) else {
                    fatalError("couldn't find expected attributes")
                }
                
                layoutAttributes.frame = attributes.frame
                sectionIndicesToAdd.insert(attributes.indexPath.section)
            }
        }
        
        sectionIndicesToAdd.subtracting(existingSectionIndices).forEach { section in
            let indexPath = IndexPath(item: 0, section: section)
            if let sectionAttributes = self.layoutAttributesForSupplementaryView(ofKind: Constant.calendarHeadertKind, at: indexPath) {
                superAttributesCopy.append(sectionAttributes)
            }
        }
        
        return superAttributesCopy
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let layoutAttributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes else {
            return nil
        }
        
        var origin = layoutAttributes.frame.origin
        origin.x = self.headerReferenceSize.width
        var size = layoutAttributes.frame.size
        size.width -= self.headerReferenceSize.width
        
        layoutAttributes.frame = CGRect(origin: origin, size: size)
        return layoutAttributes
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard elementKind == Constant.calendarHeadertKind else {
            fatalError("unknown element kind")
        }
        
        guard let collectionView = collectionView else {
            return nil
        }
        
        let contentOffsetY = collectionView.contentOffset.y + collectionView.contentInset.top
        let boundaries = verticalBoundaries(forSection: indexPath.section)
        
        let layoutAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
        
        let minimumY = boundaries.minimum
        let maximumY = boundaries.maximum - self.headerReferenceSize.height
        
        var offsetY: CGFloat
        
        if contentOffsetY < minimumY {
            offsetY = minimumY
        }
        else if contentOffsetY > maximumY {
            offsetY = maximumY
        }
        else {
            offsetY = contentOffsetY
        }
        
        let origin = CGPoint(x: 0, y: offsetY)
        layoutAttributes.frame = CGRect(origin: origin, size: self.headerReferenceSize)
        
        return layoutAttributes
    }
    
    private func verticalBoundaries(forSection section: Int) -> (minimum: CGFloat, maximum: CGFloat) {

        var result = (minimum: CGFloat(0), maximum: CGFloat(0))
        
        guard let collectionView = collectionView else {
            return result
        }
        
        let numberOfItems = collectionView.numberOfItems(inSection: section)
        guard numberOfItems > 0 else {
            return result
        }
        
        if let firstItem = layoutAttributesForItem(at: IndexPath(item: 0, section: section)),
            let lastItem = layoutAttributesForItem(at: IndexPath(item: (numberOfItems - 1), section: section)) {
            
            result.minimum = firstItem.frame.minY
            result.maximum = lastItem.frame.maxY
        }
        
        return result
    }
//    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//        guard elementKind == Constant.calendarHeadertKind else {
//            fatalError("unknown element kind")
//        }
//        
//        let cellLayoutAttributes = self.layoutAttributesForItem(at: indexPath)!
//        
//        let layoutAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: elementKind, with: indexPath)
//        
//        let origin = CGPoint(x: 0, y: cellLayoutAttributes.frame.origin.y)
//        
//        layoutAttributes.frame = CGRect(origin: origin, size: self.headerReferenceSize)
//        return layoutAttributes
//    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
