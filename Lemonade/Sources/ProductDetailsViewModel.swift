//
//  ProductDetailsViewModel.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

struct ProductDetailsViewModel {
    
    enum Cell: Int {
        case image
        case mainInfo
        case description
    }
    
    struct Constant {
        static let imageCellReuseId = "imageCellId"
        static let mainInfoCellReuseId = "mainInfoCellId"
        static let descriptionCellReuseId = "descriptionCellId"
    }
    
    let product: Product
    
    init(_ product: Product) {
        self.product = product
        
        fillModel()
    }
    
    private var tableModel = TableModel()
    private mutating func fillModel() {
        
        if product.imageHighResUrl != nil {
            tableModel.addCellId(Cell.image.rawValue, in: 0)
        }
        
        if product.name != nil && product.price != nil {
            tableModel.addCellId(Cell.mainInfo.rawValue, in: 0)
        }
        
        if product.desc != nil {
            tableModel.addCellId(Cell.description.rawValue, in: 0)
        }
    }
    
    func setup(_ cell: UITableViewCell) {
        
        switch cell {
        case let imageCell as ProductImageCell:
            guard let imageName = product.imageHighResUrl else {
                fatalError("image name is missed")
            }
            
            imageCell.productImageView.image = UIImage(named: imageName)
            
        case let mainInfoCell as ProductMainInfoCell:
            guard let name = product.name, let price = product.price else {
                fatalError("required info is missed")
            }
            
            mainInfoCell.nameLabel.text = name
            mainInfoCell.priceLabel.text = "\(price)"
            mainInfoCell.currencyLabel.text = "CHF"
            
        case let descriptionCell as ProductDescriptionCell:
            guard let description = product.desc else {
                fatalError("description is missed")
            }
            
            descriptionCell.descriptionLabel.text = description
        
        default:
            fatalError("cell type is not implemented yet")
        }
    }
    
    var numberOfSections: Int {
        return tableModel.numberOfSections
    }
    
    func numberOfRows(in section: Int) -> Int {
        return tableModel.numberOfRows(in: section)
    }
    
    func reuseIdentifier(for indexPath: IndexPath) -> String {
        guard let cellId = Cell(rawValue: tableModel.cellId(for: indexPath)) else {
            fatalError("unknown cell id")
        }
        
        switch cellId {
        case .image:
            return Constant.imageCellReuseId
        case .mainInfo:
            return Constant.mainInfoCellReuseId
        case .description:
            return Constant.descriptionCellReuseId
        }
    }
}
