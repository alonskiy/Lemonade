//
//  RoundRectButton.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable
class RoundRectButton: UIButton {
    
    @IBInspectable var radius: CGFloat = 0 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderColor : UIColor? {
        didSet {
            self.setNeedsLayout()
        }
    }

    @IBInspectable var borderWidth : CGFloat = 0 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = radius
        self.clipsToBounds = true
        
        self.layer.borderColor = self.borderColor?.cgColor
        self.layer.borderWidth = self.borderWidth
 
    }
}


