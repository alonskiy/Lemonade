//
//  ProductDetailsViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class ProductDetailsViewController: UIViewController {
    struct Constant {
        static let bottomInset = CGFloat(160)
        static let estimatedRowHeight = CGFloat(100)
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: ProductDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset.bottom = Constant.bottomInset
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = Constant.estimatedRowHeight
        tableView.allowsSelection = false
    }
}

extension ProductDetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let reuseIdentifier = viewModel.reuseIdentifier(for: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)
        
        viewModel.setup(cell)
        
        return cell
    }
}
