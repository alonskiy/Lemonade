//
//  TimelineController.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 07/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

class TimelineController: UIViewController {

    struct Constant {
        static let cellReuseIdentifier = "timelineCellId"
        static let headerReuseIdentifier = "timelineHeader"
        static let timelineHeaderNibName = "TimelineHeader"
        static let showDetailsSegue = "showPersonDetails"
    }
    
    fileprivate var allFriends: [Person] = [Person]()
    private let networkManager = NetworkManager(communicator: Communicator(), parser: Parser())
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let headerNib = UINib(nibName: Constant.timelineHeaderNibName, bundle: nil)
        collectionView.register(headerNib, forSupplementaryViewOfKind: TimelineFlowLayout.Constant.calendarHeadertKind, withReuseIdentifier: Constant.headerReuseIdentifier)
        
        loadFriends()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Constant.showDetailsSegue else { return }
        guard let detailsController = segue.destination as? ShortListViewController else { return }
        guard let personCell = sender as? TimelineCell else { return }
        guard let indexPath = collectionView.indexPath(for: personCell) else { return }
        
        detailsController.setupDependency(allFriends[indexPath.row])
    }
    
    private func loadFriends() {
        self.networkManager.getAllFriends(completion: { (friends) in
            self.allFriends.append(contentsOf: friends)
            self.collectionView.reloadData()
        })
    }
}

extension TimelineController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // TODO: read from model
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allFriends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.cellReuseIdentifier, for: indexPath) as! TimelineCell
        
        let currentFriend = allFriends[indexPath.row]
        cell.nameLabel.text = currentFriend.name
        
        if let imageName = currentFriend.picUrl {
            cell.avatarImageView.image = UIImage(named: imageName)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: TimelineFlowLayout.Constant.calendarHeadertKind, withReuseIdentifier: Constant.headerReuseIdentifier, for: indexPath) as! TimelineHeader
        header.dayLabel.text = dateTitle(for: indexPath)
        header.monthLabel.text = dayOfWeekTitle(for: indexPath)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            fatalError("unknown layout")
        }
        
        return CGSize(width: collectionView.bounds.width, height: flowLayout.itemSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // We don't need standard FlowLayout headers. Instead we use our custom ones
        return CGSize.zero
    }
}

// TODO: Don't use hard-coded values
extension TimelineController {
    fileprivate func dayOfWeekTitle(for indexPath: IndexPath) -> String {
        let result: String
        switch indexPath.section {
        case 0:
            result = "Mon"
        case 1:
            result = "Tue"
        case 2:
            result = "Wen"
        default:
            result = "Sun"
        }
        return result
    }

    fileprivate func dateTitle(for indexPath: IndexPath) -> String {
        let result: String
        switch indexPath.section {
        case 0:
            result = "12"
        case 1:
            result = "13"
        case 2:
            result = "14"
        default:
            result = "∞"
        }
        return result
    }
}
