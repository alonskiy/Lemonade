//
//  TimelineHeader.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class TimelineHeader: UICollectionReusableView {
    
    @IBOutlet weak var dayLabel: UILabel!
    // TODO: rename. it's not month label
    @IBOutlet weak var monthLabel: UILabel!

    override func prepareForReuse() {
        super.prepareForReuse()
        
        dayLabel.text = nil
        monthLabel.text = nil
    }
}
