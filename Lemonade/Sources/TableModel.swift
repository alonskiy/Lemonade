//
//  TableModel.swift
//  Eat+Drink
//
//  Created by Alexander Lonsky on 02/06/2017.
//  Copyright © 2017 NGTI. All rights reserved.
//

import UIKit


struct TableModel {

    typealias SectionId = Int
    typealias CellId = Int
    
    struct Section {
        let id: SectionId
        private(set) var cellIds: [CellId]
        
        init(with id: SectionId, cellIds cells: [CellId] = []) {
            self.id = id
            self.cellIds = cells
        }
        
        mutating func addCellId(_ id: CellId) {
            cellIds.append(id)
        }
    }
    
    private(set) var sections: [Section]
    
    // MARK: - Init
    
    init(withSections sections: [Section] = []) {
        self.sections = sections
    }
    
    // MARK: - Setup model

    mutating func addSectionId(_ id: SectionId) {
        let sectionIndex = sections.index(where: { $0.id == id })
        guard sectionIndex == nil else {
            fatalError("inconsistency: trying to add section with existing id")
        }
        
        let section = Section(with: id)
        sections.append(section)
    }
    
    mutating func addCellId(_ id: CellId, in sectionId: SectionId) {
        
        if let sectionIndex = sections.index(where: { $0.id == sectionId }) {
            var existingSection = sections[sectionIndex]
            existingSection.addCellId(id)
            sections[sectionIndex] = existingSection
        }
        else {
            let section = Section(with: sectionId, cellIds: [id])
            sections.append(section)
        }
    }
    
    mutating func clear() {
        sections.removeAll()
    }
    
    // MARK: - Access model
    
    var numberOfSections: Int {
        return sections.count
    }
    
    func numberOfRows(in sectionIndex: Int) -> Int {
        let section = sections[sectionIndex]
        return section.cellIds.count
    }
    
    func indexPath(for cellId: CellId) -> IndexPath? {
        for (sectionIndex, section) in sections.enumerated() {
            for (cellIndex, cell) in section.cellIds.enumerated() {
                if cell == cellId {
                    return IndexPath(row: cellIndex, section: sectionIndex)
                }
            }
        }
        return nil
    }
    
    func index(for sectionId: SectionId) -> Int? {
        return sections.index(where: { $0.id == sectionId })
    }
    
    func cellId(for indexPath: IndexPath) -> CellId {
        guard indexPath.section < sections.count else {
            fatalError("inconsistency: section index out of bounds")
        }
        
        let section = sections[indexPath.section]
        
        guard indexPath.row < section.cellIds.count else {
            fatalError("inconsistency: cell index out of bounds")
        }
        
        return section.cellIds[indexPath.row]
    }

    func sectionId(for index: Int) -> SectionId {
        guard index < sections.count else {
            fatalError("inconsistency: section index out of bounds")
        }

        let section = sections[index]
        return section.id
    }
}
