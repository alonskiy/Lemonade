//
//  RoundImageView.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class RoundImageView: UIImageView {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.masksToBounds = true
        self.layer.cornerRadius = min(self.bounds.width, self.bounds.height) / 2
    }
}

