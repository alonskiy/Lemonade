//
//  ShortListCell.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

protocol ShortListCellDelegate {
    func onBuy(shortListCell: ShortListCell)
}


class ShortListCell: UITableViewCell {
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    
    var delegate: ShortListCellDelegate?
    
    @IBAction func onBuy(_ sender: Any) {
        delegate?.onBuy(shortListCell: self)
    }
}
