//
//  FavList.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 09/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

final class FavList {
    
    // Can't init is singleton
    private init() { }
    
    // MARK: Shared Instance
    
    static let sharedInstance = FavList()
    
    var list = [Product]()
    
    func isProductInTheList(_ product: Product) -> Bool {
        for currentProduct in list {
            if let currentProductId = currentProduct.productId, let productId = product.productId, currentProductId == productId {
                return true
            }
        }
        return false
    }
}
