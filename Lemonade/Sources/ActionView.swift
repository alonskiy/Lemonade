//
//  ActionView.swift
//  Lemonade
//
//  Created by Tsvetomir Stanchev on 11/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import UIKit

protocol ActionViewDelegate {
    func tapDidOccur(for actionView: ActionView)
}

@IBDesignable class ActionView: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var actionImageView: UIImageView!
    
    @IBOutlet weak var actionLabel: UILabel!
    
    @IBInspectable var actionText: String = "" {
        didSet {
            self.actionLabel.text = self.actionText
        }
    }
    
    @IBInspectable var actionImage: UIImage?
    
    @IBInspectable var actionImageSelected: UIImage?
    
    @IBInspectable var selected: Bool = false {
        didSet {
            if self.selected {
                self.view.backgroundColor = UIColor(red: 0.137, green: 0.741, blue: 0.862, alpha: 1.0)
                self.actionImageView.image = self.actionImageSelected
            } else {
                self.view.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                self.actionImageView.image = self.actionImage
            }
        }
    }

    var delegate: ActionViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // MARK: - Private Helper Methods
    
    // Performs the initial setup.
    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        
        // Show the view.
        addSubview(view)
        
        //create a gesture recognizer (tap gesture)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        
        //add the gesture recognizer to a view
        view.addGestureRecognizer(tapGesture)
    }
    
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return view
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        self.selected = !self.selected
        delegate?.tapDidOccur(for: self)
    }

}
