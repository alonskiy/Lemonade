//
//  QuizStepOneViewController.swift
//  Lemonade
//
//  Created by Alexander Lonsky on 08/06/2017.
//  Copyright © 2017 Tsvetomir Stanchev. All rights reserved.
//

import Foundation
import UIKit

class QuizStepOneViewController: UIViewController {
    
    struct Constant {
        static let segue = "quizMissedDataSegue"
    }
    
    var person: Person!
    
    @IBOutlet weak var questionLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        questionLabel.text = self.question
        personalityImageView.image = self.faceImage
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == Constant.segue, let controller = segue.destination as? QuizStepTwoViewController else {
            return
        }
        
        controller.person = person
    }
    
    private var faceImage: UIImage? {
        guard let gender = person?.gender else {
            return nil
        }
        
        switch gender {
        case .male:
            return #imageLiteral(resourceName: "male")
        case .female:
            return #imageLiteral(resourceName: "female")
        }
    }
    
    private var question: String {
        
        guard let gender = person?.gender else {
            return "What kind of person is it?"
        }
        
        switch gender {
        case .male:
            return "What kind of person is he?"
        case .female:
            return "What kind of person is she?"
        }
    }
    
    @IBAction func onNatureSliderValueChanged(_ sender: Any) {
        if (sender as! UISlider).value > 0.5 {
            personalityImageView.image = self.faceImage
        } else {
            personalityImageView.image = #imageLiteral(resourceName: "astronaut")
        }
    }
    @IBOutlet weak var personalityImageView: UIImageView!
    
    @IBAction func onCancelButtonDidPress(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }
}
